# nginx_rev_proxy

Simple Ansible Role for configuring an Nginx-based Reverse Proxy.

# Role Variables

A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well.

# Dependencies

This role has been designed to work together with
[certbot-gandi](https://codeberg.org/Prezu/certbot-gandi), but it can be used
without it, too.

# Example Playbooks

This section demonstrates how this role can be used. For all the examples,
you have to set up a DNS A or AAAA record to point to the server on which
you're setting up the Nginx rev proxy.

## Simple use-case for public site

If you're not interested in restricting access to only certain CIDRs, you just:

```yaml
  roles:
    - role: nginx_rev_proxy
      vars:
        host_name: "some-host"
        domain_name: "example.com"
        endpoint: "http://localhost:8080"
        ssl_cert_full_path: "/etc/ssh/mycert.pem"
        ssl_private_key_full_path: "/etc/ssh/mykey.pem"
```

This will set up the Rev Proxy that points `http://localhost:8080`, when the
user points their browser to `https://some-host.example.com`.

Additionally, if you've used
[certbot-gandi](https://codeberg.org/Prezu/certbot-gandi) to set up Let's
Encrypt to give you the Certificate and Private Key PEM files, you can skip
`ssl_cert_full_path` and `ssl_private_key_full_path`:

```yaml
  roles:
    - role: nginx_rev_proxy
      vars:
        host_name: "some-host"
        domain_name: "example.com"
        endpoint: "http://localhost:8080"
```

## Local Only

If you're running a Nginx serving sites to a public Internet, but you want to
restrict access to your new subdomain to e.g. your home LAN only:

```yaml
  roles:
    - role: nginx_rev_proxy
      vars:
        host_name: "some-host"
        domain_name: "example.com"
        endpoint: "http://localhost:8080"
        ssl_cert_full_path: "/etc/ssh/mycert.pem"
        ssl_private_key_full_path: "/etc/ssh/mykey.pem"
        local_only: true
        locals_allowed:
          - "192.168.0.0/24"
```

As in the previous example, you can skip `*_pem` variables if you're already
using [certbot-gandi](https://codeberg.org/Prezu/certbot-gandi).

# License

GPLv3
